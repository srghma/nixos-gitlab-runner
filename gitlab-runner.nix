/*
 * An opinonated Gitlab-runner, that allows for nix builds (with caching)
 * on NixOS build machines
 */

/*
* How to use:
*
* services.gitlab-runner2 = {
*   enable = true;
*   registrationConfigFile = builtins.toFile "gitlab-runner-secret.conf" ''
*     CI_SERVER_URL=https://gitlab.mycompany.com/
*     REGISTRATION_TOKEN=mytoken
*
*     # for docker:dind - enable priviliged mode to allow connect to
*     DOCKER_PRIVILEGED=true
*
*     # for docker:dind - with this docker:dind will use 2375 port (with http) instead of 2376 port (with https)
*     RUNNER_ENV="DOCKER_TLS_CERTDIR="
*
*     # allow runner to be enabled for other gitlab projects too
*     REGISTER_LOCKED=false
*   '';
* };
*/

/*
* To redeploy:
*
* 1. Disable gitlab-runner
*
* services.gitlab-runner2.enable = false
*
* 2. Ssh and remove cache
*
* $ sudo rm -rfd /var/lib/gitlab-runner && sudo rm -rfd /etc/gitlab-runner
*
* 3. Enable gitlab-runner again
*
* services.gitlab-runner2.enable = true
*/


{ config, pkgs, lib, ...}:
with lib;
let
  cfg = config.services.gitlab-runner2;
in
  {
    options.services.gitlab-runner2 = {
      enable = lib.mkEnableOption "Gitlab Runner";
      registrationConfigFile = lib.mkOption {
        description = ''
          Configuration file used got gitlab-runner registration.
          It is a list of environment variables.
          A list of all supported environment variables can be found
          in
             gitlab-runner register --help

          One that you probably want to set is
          CI_SERVER_URL=<CI server URL>
          REGISTRATION_TOKEN=<registration secret>

        '';
        type = lib.types.path;
      };

      gracefulTermination = mkOption {
        default = false;
        type = types.bool;
        description = ''
          Finish all remaining jobs before stopping, restarting or reconfiguring.
          If not set gitlab-runner will stop immediatly without waiting for jobs to finish,
          which will lead to failed builds.
        '';
      };

      gracefulTimeout = mkOption {
        default = "infinity";
        type = types.str;
        example = "5min 20s";
        description = ''Time to wait until a graceful shutdown is turned into a forceful one.'';
      };

      workDir = mkOption {
        default = "/var/lib/gitlab-runner";
        type = types.path;
        description = "The working directory used";
      };

      package = mkOption {
        description = "Gitlab Runner package to use";
        default = pkgs.gitlab-runner;
        defaultText = "pkgs.gitlab-runner";
        type = types.package;
        example = literalExample "pkgs.gitlab-runner_1_11";
      };

      packages = mkOption {
        default = [ pkgs.bash pkgs.docker-machine ];
        defaultText = "[ pkgs.bash pkgs.docker-machine ]";
        type = types.listOf types.package;
        description = ''
          Packages to add to PATH for the gitlab-runner process.
        '';
      };
    };
    config = mkIf cfg.enable {
      systemd.services.gitlab-runner2 = {
        path = cfg.packages;
        environment = config.networking.proxy.envVars;
        description = "Gitlab Runner";
        after = [ "network.target" "docker.service"];
        requires = ["docker.service"];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          EnvironmentFile = "${cfg.registrationConfigFile}";
          ExecStartPre = ''${cfg.package.bin}/bin/gitlab-runner register \
            --non-interactive=true \
            --name gitlab-runner \
            --executor "docker" \
            --docker-image "alpine" \
            --docker-disable-cache=true
          '';
          ExecStart = ''${cfg.package.bin}/bin/gitlab-runner run \
            --working-directory ${cfg.workDir} \
            --user gitlab-runner \
            --service gitlab-runner
          '';
          ExecStopPost = ''${cfg.package.bin}/bin/gitlab-runner unregister \
            --name gitlab-runner
          '';

        } //  optionalAttrs (cfg.gracefulTermination) {
          TimeoutStopSec = "${cfg.gracefulTimeout}";
          KillSignal = "SIGQUIT";
          KillMode = "process";
        };
      };

      virtualisation.docker.enable = true;

      # Make the gitlab-runner command availabe so users can query the runner
      environment.systemPackages = [ cfg.package ];

      users.users.gitlab-runner = {
        group = "gitlab-runner";
        extraGroups = ["docker"];
        uid = config.ids.uids.gitlab-runner;
        home = cfg.workDir;
        createHome = true;
      };

      users.groups.gitlab-runner.gid = config.ids.gids.gitlab-runner;
    };
  }
